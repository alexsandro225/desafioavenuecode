# new feature
# Tags: optional
    
Feature: US1 - Create Task
         As a ToDo App user
         I should be able to create a task
         So I can manage my tasks

Scenario: The user should always see the 'My Tasks' link on the NavBar
         Given that I openned the website "https://qa-test.avenuecode.com/"
         When I click on the link Sign In
         And I type my e-mail
         And I type my password
         And I click on the button Sign In
         Then I should see the "My Tasks" link on the NavBar

Scenario: The user should see a message on the top part saying that list belongs to the logged user
        Given that I am on the application logged as a user
        When I click on My Tasks button
        Then I should see the message "Hey Alexsandro Oliveira da Silva, this is your todo list for today"

Scenario: The user should be able to enter a new task by hitting enter or clicking on the add task button.
         Given that I am on the application logged as a user alexsandro
         When I click on My Tasks button
         And I type some text in the new task field
         And I click on the task add button
         Then I should see the new task appended on the list of created tasks

Scenario: The task should require at least three characters, so the user can enter it.
          Given that I am on the application logged as a user 03
          When I click on My Tasks button 03
          And I type 2 characters in the new task field
          And I click on the task add button b
          Then the application should not show a new task created

Scenario: The task can't have more than 250 characters
          Given that I am on the application logged as a user a
          When I click on My Tasks button b
          And I type 251 characters in the new task field c
          And I click on the task add button d
          Then the application should not show a new task created e
