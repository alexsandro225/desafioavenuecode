package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        format = {"pretty","html:target/"},
        features = {"src/test/java/features"},
        glue = {"steps"},
        dryRun = false,
        monochrome = false
)
public class runTest{}