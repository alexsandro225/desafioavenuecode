package steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import support.FirstPage;
import support.LoginForm;
import support.LoginPage;
import support.Web;

public class feature_01_scenario_01 {
    WebDriver driver;


    @Given("^that I openned the website \"([^\"]*)\"$")
    public void that_I_openned_the_website(String arg1) throws Throwable {
        this.driver = Web.createChrome();
    }

    @When("^I click on the link Sign In$")
    public void i_click_on_the_link_Sign_In() throws Throwable {
        new LoginPage(driver).clickSignInButton();
    }

    @When("^I type my e-mail$")
    public void i_type_my_e_mail() throws Throwable {
        new LoginForm(driver)
                .typeEmail("alexsandro225@gmail.com");
    }

    @When("^I type my password$")
    public void i_type_my_password() throws Throwable {
        new LoginForm(driver)
                .typePassword("123avenuecode4");
    }

    @When("^I click on the button Sign In$")
    public void i_click_on_the_button_Sign_In() throws Throwable {
        new LoginForm(driver)
                .clickSubmitSignInButton();
    }

    @Then("^I should see the \"([^\"]*)\" link on the NavBar$")
    public void i_should_see_the_link_on_the_NavBar(String arg1) throws Throwable {
        Assert.assertEquals(arg1, new FirstPage(driver).getMessage());
    }
}
