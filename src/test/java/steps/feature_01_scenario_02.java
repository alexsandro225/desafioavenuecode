package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import support.*;
import cucumber.api.java.Before;


public class feature_01_scenario_02 {
    WebDriver driver;

    public feature_01_scenario_02(){
        this.driver = Web.createChrome();
    }

    @Given("^that I am on the application logged as a user$")
    public void that_I_am_on_the_application_logged_as_a_user() throws Throwable {
        new LoginPage(driver)
                .clickSignInButton()
                .typeEmail("alexsandro225@gmail.com")
                .typePassword("123avenuecode4")
                .clickSubmitSignInButton();
    }

    @When("^I click on My Tasks button$")
    public void i_click_on_My_Tasks_button() throws Throwable {
         new FirstPage(driver)
                .clickOnMyTaskButton();
    }

    @Then("^I should see the message \"([^\"]*)\"$")
    public void i_should_see_the_message(String arg1) throws Throwable {
        Assert.assertEquals(arg1 , new myTaskPage(driver).getMessageOnTop());
    }
}
