package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import support.FirstPage;
import support.LoginPage;
import support.Web;
import support.myTaskPage;

public class feature_01_scenario_03 {
    WebDriver driver;

    public feature_01_scenario_03(){
        this.driver = Web.createChrome();
    }

    @Given("^that I am on the application logged as a user alexsandro$")
    public void that_I_am_on_the_application_logged_as_a_user_alexsandro() throws Throwable {
        new LoginPage(driver)
                .clickSignInButton()
                .typeEmail("alexsandro225@gmail.com")
                .typePassword("123avenuecode4")
                .clickSubmitSignInButton();
    }

    @When("^I click on My Tasks button 02$")
    public void i_click_on_My_Tasks_button() throws Throwable {
        new FirstPage(driver)
                .clickOnMyTaskButton();
    }

    @When("^I type some text in the new task field$")
    public void i_type_some_text_in_the_new_task_field() throws Throwable {
        new FirstPage(driver)
                .clickOnMyTaskButton().typeTextOnNewTaskForm("something");
    }

    @When("^I click on the task add button$")
    public void i_click_on_the_task_add_button() throws Throwable {
        new myTaskPage(driver)
                .submitNewTaskForm();
    }

    @Then("^I should see the new task appended on the list of created tasks$")
    public void i_should_see_the_new_task_appended_on_the_list_of_created_tasks() throws Throwable {
        String task = new myTaskPage(driver).getNameTask();
        Assert.assertEquals("something", task);
    }

}
