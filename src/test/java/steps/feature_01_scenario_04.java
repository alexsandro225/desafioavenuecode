package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import support.FirstPage;
import support.LoginPage;
import support.Web;
import support.myTaskPage;

public class feature_01_scenario_04 {
    WebDriver driver;

    public feature_01_scenario_04(){
        this.driver = Web.createChrome();
    }

    @Given("^that I am on the application logged as a user (\\d+)$")
    public void that_I_am_on_the_application_logged_as_a_user(int arg1) throws Throwable {
        new LoginPage(driver)
                .clickSignInButton()
                .loginIn("alexsandro225@gmail.com","123avenuecode4");
    }

    @When("^I click on My Tasks button (\\d+)$")
    public void i_click_on_My_Tasks_button(int arg1) throws Throwable {
        new FirstPage(driver)
                .clickOnMyTaskButton();
    }

    @When("^I type (\\d+) characters in the new task field$")
    public void i_type_characters_in_the_new_task_field(int arg1) throws Throwable {
        new myTaskPage(driver).typeTextOnNewTaskForm("ab");
    }

    @When("^I click on the task add button b$")
    public void i_click_on_the_task_add_button() throws Throwable {
        new myTaskPage(driver)
                .submitNewTaskForm();
    }

    @Then("^the application should not show a new task created$")
    public void the_application_should_not_show_a_new_task_created() throws Throwable {
        String elemento = new myTaskPage(driver).getNameTask();
        Assert.assertEquals("", elemento);
    }
}
