package support;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FirstPage {
    WebDriver driver;

    public FirstPage(WebDriver driver){
        this.driver = driver;
    }

    public String getMessage (){
        String message = driver.findElement(By.linkText("My Tasks")).getText();

        return message;
    }

    public myTaskPage clickOnMyTaskButton(){
        driver.findElement(By.linkText("My Tasks")).click();

        return new myTaskPage(driver);
    }
}
