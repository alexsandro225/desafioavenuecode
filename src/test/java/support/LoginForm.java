package support;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginForm {
    WebDriver driver;

    public LoginForm(WebDriver driver){
        this.driver = driver;
    }

    public LoginForm typeEmail (String email){
        driver.findElement(By.id("user_email")).sendKeys(email);

        return this;
    }

    public LoginForm typePassword (String senha){
        driver.findElement(By.id("user_password")).sendKeys(senha);

        return this;
    }

    public FirstPage clickSubmitSignInButton (){
        driver.findElement(By.name("commit")).click();

        return new FirstPage(driver);
    }

    public FirstPage loginIn(String login, String password){
        typeEmail(login)
        .typePassword(password)
        .clickSubmitSignInButton();

        return new FirstPage(driver);
    }
}
