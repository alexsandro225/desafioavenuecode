package support;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginPage {
    WebDriver driver;

    public LoginPage (WebDriver driver){
        this.driver = driver;
    }

    public LoginForm clickSignInButton(){
        driver.findElement(By.linkText("Sign In")).click();

        return new LoginForm(driver);
    }
}
