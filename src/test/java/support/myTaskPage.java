package support;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class myTaskPage {
    WebDriver driver;

    public myTaskPage(WebDriver driver){
        this.driver = driver;
    }

    public String getMessageOnTop(){
        String message = driver.findElement(By.xpath("/html/body/div[1]/h1")).getText();

        return message;
    }

    public myTaskPage typeTextOnNewTaskForm (String texto){
        driver.findElement(By.id("new_task")).sendKeys(texto);

        return this;
    }

    public myTaskPage submitNewTaskForm (){
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[2]/span")).click();

        return this;
    }

    public String getNameTask(){
        String textTask = driver.findElement(By.partialLinkText("something")).getText();
        return textTask;
    }
}
