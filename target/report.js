$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("createTask.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "# new feature"
    },
    {
      "line": 2,
      "value": "# Tags: optional"
    }
  ],
  "line": 4,
  "name": "US1 - Create Task",
  "description": "       As a ToDo App user\r\n       I should be able to create a task\r\n       So I can manage my tasks",
  "id": "us1---create-task",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 9,
  "name": "The user should always see the \u0027My Tasks\u0027 link on the NavBar",
  "description": "",
  "id": "us1---create-task;the-user-should-always-see-the-\u0027my-tasks\u0027-link-on-the-navbar",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 10,
  "name": "that I openned the website \"https://qa-test.avenuecode.com/\"",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I click on the link Sign In",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "I type my e-mail",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I type my password",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I click on the button Sign In",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I should see the \"My Tasks\" link on the NavBar",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "https://qa-test.avenuecode.com/",
      "offset": 28
    }
  ],
  "location": "feature_01_scenario_01.that_I_openned_the_website(String)"
});
formatter.result({
  "duration": 34179541349,
  "status": "passed"
});
formatter.match({
  "location": "feature_01_scenario_01.i_click_on_the_link_Sign_In()"
});
formatter.result({
  "duration": 1490394879,
  "status": "passed"
});
formatter.match({
  "location": "feature_01_scenario_01.i_type_my_e_mail()"
});
formatter.result({
  "duration": 1332967974,
  "status": "passed"
});
formatter.match({
  "location": "feature_01_scenario_01.i_type_my_password()"
});
formatter.result({
  "duration": 307300148,
  "status": "passed"
});
formatter.match({
  "location": "feature_01_scenario_01.i_click_on_the_button_Sign_In()"
});
formatter.result({
  "duration": 1443092438,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "My Tasks",
      "offset": 18
    }
  ],
  "location": "feature_01_scenario_01.i_should_see_the_link_on_the_NavBar(String)"
});
formatter.result({
  "duration": 172552191,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "The user should see a message on the top part saying that list belongs to the logged user",
  "description": "",
  "id": "us1---create-task;the-user-should-see-a-message-on-the-top-part-saying-that-list-belongs-to-the-logged-user",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 18,
  "name": "that I am on the application logged as a user",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "I click on My Tasks button",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "I should see the message \"Hey Alexsandro Oliveira da Silva, this is your todo list for today\"",
  "keyword": "Then "
});
formatter.match({
  "location": "feature_01_scenario_02.that_I_am_on_the_application_logged_as_a_user()"
});
formatter.result({
  "duration": 41394549800,
  "status": "passed"
});
formatter.match({
  "location": "feature_01_scenario_02.i_click_on_My_Tasks_button()"
});
formatter.result({
  "duration": 1448350450,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Hey Alexsandro Oliveira da Silva, this is your todo list for today",
      "offset": 26
    }
  ],
  "location": "feature_01_scenario_02.i_should_see_the_message(String)"
});
formatter.result({
  "duration": 62861368,
  "error_message": "org.junit.ComparisonFailure: expected:\u003c[Hey Alexsandro Oliveira da Silva, this is your todo list for today]\u003e but was:\u003c[Alexsandro Oliveira da Silva\u0027s ToDo List]\u003e\r\n\tat org.junit.Assert.assertEquals(Assert.java:115)\r\n\tat org.junit.Assert.assertEquals(Assert.java:144)\r\n\tat steps.feature_01_scenario_02.i_should_see_the_message(feature_01_scenario_02.java:36)\r\n\tat ✽.Then I should see the message \"Hey Alexsandro Oliveira da Silva, this is your todo list for today\"(createTask.feature:20)\r\n",
  "status": "failed"
});
formatter.scenario({
  "line": 22,
  "name": "The user should be able to enter a new task by hitting enter or clicking on the add task button.",
  "description": "",
  "id": "us1---create-task;the-user-should-be-able-to-enter-a-new-task-by-hitting-enter-or-clicking-on-the-add-task-button.",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 23,
  "name": "that I am on the application logged as a user alexsandro",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "I click on My Tasks button",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "I type some text in the new task field",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "I click on the task add button",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "I should see the new task appended on the list of created tasks",
  "keyword": "Then "
});
formatter.match({
  "location": "feature_01_scenario_03.that_I_am_on_the_application_logged_as_a_user_alexsandro()"
});
formatter.result({
  "duration": 27973770664,
  "status": "passed"
});
formatter.match({
  "location": "feature_01_scenario_02.i_click_on_My_Tasks_button()"
});
formatter.result({
  "duration": 15950630277,
  "status": "passed"
});
formatter.match({
  "location": "feature_01_scenario_03.i_type_some_text_in_the_new_task_field()"
});
formatter.result({
  "duration": 1918291469,
  "status": "passed"
});
formatter.match({
  "location": "feature_01_scenario_03.i_click_on_the_task_add_button()"
});
formatter.result({
  "duration": 174735367,
  "status": "passed"
});
formatter.match({
  "location": "feature_01_scenario_03.i_should_see_the_new_task_appended_on_the_list_of_created_tasks()"
});
formatter.result({
  "duration": 150693057,
  "status": "passed"
});
formatter.scenario({
  "line": 29,
  "name": "The task should require at least three characters, so the user can enter it.",
  "description": "",
  "id": "us1---create-task;the-task-should-require-at-least-three-characters,-so-the-user-can-enter-it.",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 30,
  "name": "that I am on the application logged as a user 03",
  "keyword": "Given "
});
formatter.step({
  "line": 31,
  "name": "I click on My Tasks button 03",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "I type 2 characters in the new task field",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "I click on the task add button b",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "the application should not show a new task created",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "03",
      "offset": 46
    }
  ],
  "location": "feature_01_scenario_04.that_I_am_on_the_application_logged_as_a_user(int)"
});
formatter.result({
  "duration": 18124936280,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "03",
      "offset": 27
    }
  ],
  "location": "feature_01_scenario_04.i_click_on_My_Tasks_button(int)"
});
formatter.result({
  "duration": 1059976838,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 7
    }
  ],
  "location": "feature_01_scenario_04.i_type_characters_in_the_new_task_field(int)"
});
formatter.result({
  "duration": 148708312,
  "status": "passed"
});
formatter.match({
  "location": "feature_01_scenario_04.i_click_on_the_task_add_button()"
});
formatter.result({
  "duration": 433829441,
  "status": "passed"
});
formatter.match({
  "location": "feature_01_scenario_04.the_application_should_not_show_a_new_task_created()"
});
formatter.result({
  "duration": 163658260,
  "error_message": "org.junit.ComparisonFailure: expected:\u003c[]\u003e but was:\u003c[something]\u003e\r\n\tat org.junit.Assert.assertEquals(Assert.java:115)\r\n\tat org.junit.Assert.assertEquals(Assert.java:144)\r\n\tat steps.feature_01_scenario_04.the_application_should_not_show_a_new_task_created(feature_01_scenario_04.java:47)\r\n\tat ✽.Then the application should not show a new task created(createTask.feature:34)\r\n",
  "status": "failed"
});
formatter.scenario({
  "line": 36,
  "name": "The task can\u0027t have more than 250 characters",
  "description": "",
  "id": "us1---create-task;the-task-can\u0027t-have-more-than-250-characters",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 37,
  "name": "that I am on the application logged as a user a",
  "keyword": "Given "
});
formatter.step({
  "line": 38,
  "name": "I click on My Tasks button b",
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "I type 251 characters in the new task field c",
  "keyword": "And "
});
formatter.step({
  "line": 40,
  "name": "I click on the task add button d",
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "the application should not show a new task created e",
  "keyword": "Then "
});
formatter.match({
  "location": "feature_01_scenario_05.that_I_am_on_the_application_logged_as_a_user_a()"
});
formatter.result({
  "duration": 23254372767,
  "status": "passed"
});
formatter.match({
  "location": "feature_01_scenario_05.i_click_on_My_Tasks_button_b()"
});
formatter.result({
  "duration": 1140950232,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "251",
      "offset": 7
    }
  ],
  "location": "feature_01_scenario_05.i_type_characters_in_the_new_task_field_c(int)"
});
formatter.result({
  "duration": 2522046874,
  "status": "passed"
});
formatter.match({
  "location": "feature_01_scenario_05.i_click_on_the_task_add_button_d()"
});
formatter.result({
  "duration": 158241587,
  "status": "passed"
});
formatter.match({
  "location": "feature_01_scenario_05.the_application_should_not_show_a_new_task_created_e()"
});
formatter.result({
  "duration": 91820851,
  "error_message": "org.junit.ComparisonFailure: expected:\u003c[]\u003e but was:\u003c[something]\u003e\r\n\tat org.junit.Assert.assertEquals(Assert.java:115)\r\n\tat org.junit.Assert.assertEquals(Assert.java:144)\r\n\tat steps.feature_01_scenario_05.the_application_should_not_show_a_new_task_created_e(feature_01_scenario_05.java:47)\r\n\tat ✽.Then the application should not show a new task created e(createTask.feature:41)\r\n",
  "status": "failed"
});
});